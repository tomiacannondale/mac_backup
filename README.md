# mac用バックアップ #

## 前準備 ##

BACKUPというUSBメモリを作成する。(ファイルシステムは「Mac OS拡張(ジャーナリング)」にする)

## ファイル構成 ##

* README.md このファイル
* backup.sh スクリプト
* exclude_files 共通的なバックアップの除外リスト

## 起動方法 ##

    ./bachup.sh
