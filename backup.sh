DEST="/Volumes/DS215-share/backup"

rsync -avz -8 --delete --include '.idm-db.gpg' --exclude '.*' --exclude 'Virtual Machines.localized' --exclude '$RECYCLE.BIN' --exclude-from exclude_files /Users/tomi/Documents/ ${DEST}/Documents
rsync -avz -8 --delete --exclude-from exclude_files /Users/tomi/.emacs.d/ ${DEST}/dot.emacs.d
rsync -avz -8 --delete --exclude-from exclude_files /Users/tomi/.ssh/ ${DEST}/dot.ssh
rsync -avz -8 --delete --exclude '.git' --exclude-from exclude_files /Users/tomi/.zsh.d/ ${DEST}/dot.zsh.d
rsync -avz -8 --delete --exclude 'vendor' --exclude 'node_modules' --exclude 'tmp' --exclude-from exclude_files /Users/tomi/code ${DEST}/
rsync -avz -8 --delete --exclude 'vendor' --exclude 'tmp' --exclude-from exclude_files /Users/tomi/dockers ${DEST}/
